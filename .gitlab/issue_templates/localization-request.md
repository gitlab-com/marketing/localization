### Localization Request
<!--Please note this is a work in progress-->
Use this template when making a new request for translation/localization to the Translated team.

## Submitter Checklist
* [ ] Name this issue descriptively by adding a `<clear and concise description / campaign name / original issue or epic name  page title / document name / blog post title` for example, "Siemens Case Study", or "gitlab-duo page for Gitlab Duo Enterprise launch", etc.
* [ ] Due date: Assume a 3-7 day turnaround (pending Translated SLA). 
* Locales: These are the languages for which you need the file(s) translated. Please **delete the ones you don't need**.
   * French (France)

/label ~"fr-FR"

   * Germany (Germany)

/label ~"de-DE"

   * Japanese (Japan)

/label ~"ja-JP"

   * Spanish (international)

/label ~"es-International"

   * Italian (Italy)

/label ~"it-IT"

   * Portuguese (Brazil)

/label ~"pt-BR"

   * Other (please specify and note this is subject to approval):

* **Provide logistical and contextual details:**
   - [ ] Submitted asset to be translated: 
      * What kind of asset is this? Video, web page, slide deck, email, form, etc.
      * Where is it used and how? Please provide some context to how this asset is used 
      * If it's a web page, please provide the URL
      * If you are requesting the translation of a PDF file, please provide an editable version of the file (InDesign, Word document, etc.). For InDesign files, this is often a package if an IDML file, as well as links, images, fonts, etc.
   - [ ] Requested file format on delivery (for example, source file is a Google doc but you want a .docx as a translated target file): 
   - [ ] GitLab approvers (please list GitLab handles):
   - [ ] If this is for review only, please link to or provide a copy of the original English document 
   - [ ] Attach reference files to this issue (English source and other material as helpful context)
   - [ ] Grant access to the source files to the Translated team's alias, gitlab@translated.com

## Translated Checklist 

* [ ] Translated Project Identifier (PID): 
* [ ] Deliverables:

/confidential
/label ~"L10n-status::New Request"
/assign @katia2
