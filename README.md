[Globalization & Localization Team](https://about.gitlab.com/handbook/marketing/localization/) uses this project for:

- Intake of localization and translation requests from GitLab teams and stakeholders through this [request template](https://gitlab.com/gitlab-com/localization/issue-tracker/-/issues/new?issuable_template=localization-request)
- Creating and managing requests on behalf of stakeholders, following the intake from the #localization Slack channel
- Operational discussions and communication with our [external LSP (language service provider) partners](https://gitlab.com/groups/gitlab-com/localization/-/epics/60)

The external LSP (language service provider) partners, [Translated](https://gitlab.com/groups/gitlab-com/localization/-/epics/11) and [Argos Multilingual](https://gitlab.com/gitlab-com/localization/localization-team/-/issues/60), offer various localization services, including performing human translation work at scale (volumes of content, number of languages, deadlines, etc.). Project managers from both external partners are members in the issue-tracker project. 



